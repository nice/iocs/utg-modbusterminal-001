require modbus
require modbusterminal
require essioc

# -----------------------------------------------------------------------------
# Configure ENVIRONMENT
# -----------------------------------------------------------------------------
# Default paths to locate database and protocol
#epicsEnvSet("STREAM_PROTOCOL_PATH", "$(beckhoff_mod_DIR)db/")
#epicsEnvSet("EPICS_DB_INCLUDE_PATH", "$(beckhoff_mod_DIR)db/")


# -----------------------------------------------------------------------------
# Asyn IP configuration
# -----------------------------------------------------------------------------
drvAsynIPPortConfigure("Bck","172.30.38.119:502",0,0,1)

# -----------------------------------------------------------------------------
# Modbus configuration
modbusInterposeConfig("Bck",0,1000,0)

# -----------------------------------------------------------------------------
#drvModbusAsynConfigure(portName, 
#                       tcpPortName,
#                       slaveAddress, 
#                       modbusFunction, 
#                       modbusStartAddress, 
#                       modbusLength,
#                       dataType,
#                       pollMsec, 
#                       plcType);

# -----------------------------------------------------------------------------
# HoldingRegisterPort
# -----------------------------------------------------------------------------
#drvModbusAsynConfigure(HoldingRegisterPort,Bck,0,4,4,4,0,1000,"Beckhoff0");


# -----------------------------------------------------------------------------
# WriteMultipleRegisterPort
# -----------------------------------------------------------------------------
#drvModbusAsynConfigure(WriteMultipleRegisterPort,Bck,0,16,2048,4,0,1000,"Beckhoff2");

# -----------------------------------------------------------------------------
# ReadMultipleRegisterPort
# -----------------------------------------------------------------------------
drvModbusAsynConfigure(ReadMultipleRegisterPort,Bck,0,4,0,16,0,1000,"Beckhoff");

# -----------------------------------------------------------------------------
# Load record instances

dbLoadRecords("read_mod.db","P=LOKI:, PORT=modbus-stream, R=SmplTmp-IOC-001:, A=-1")

iocshLoad("$(essioc_DIR)/common_config.iocsh")

iocInit()

